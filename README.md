https://minikube.sigs.k8s.io/docs/start/

Download minikube
`./download-exe.sh`

`minikube start`

`alias kubectl="minikube kubectl --"`

`kubectl get pod -A`


`minikube dashboard`


```
kubectl create namespace
kubectl -n demo apply -f deployment/001_deployment.yml
kubectl -n demo apply -f deployment/002_service.yml
#kubectl -n port-forward service/hello-minikube 7080:8080
```
