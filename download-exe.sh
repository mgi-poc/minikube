#!/bin/bash
set -e

_SCRIPT_DIR=`pwd`

mkdir -p bin

curl -L -o bin/minikube.exe        'https://github.com/kubernetes/minikube/releases/download/v1.26.0/minikube-windows-amd64.exe'
curl -L -o bin/minikube.exe.sha256 'https://github.com/kubernetes/minikube/releases/download/v1.26.0/minikube-windows-amd64.exe.sha256'

chmod +x bin/minikube.exe

echo "$(cat bin/minikube.exe.sha256) bin/minikube.exe" | sha256sum --check --status

echo "Add this location to path env-variable: $_SCRIPT_DIR/bin"
